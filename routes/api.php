<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/signup','User@SignUp');
Route::post('/login','User@Login');
Route::get('/nickname','User@GetNickName')->middleware('tokencheck');
Route::post('/nickname','User@SetNickName')->middleware('tokencheck');

Route::get('/phone','CallData@GetCallData')->middleware('tokencheck');
Route::post('/phone','CallData@SetCallData')->middleware('tokencheck');

Route::get('/profileimage','ImageData@GetProfileImageData')->middleware('tokencheck');
Route::post('/profileimage','ImageData@UpdateProfileImageData')->middleware('tokencheck');

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class User extends Controller
{
    public function Login(Request $request){
      // 토큰을 발급하는데 이 토큰은 id를 sha256 으로 해싱한다음, pk를 앞에 붙이고 .을 찍는다.
      // 그뒤 base64 encoding 해서 감싼다.
      //
      $id = $request->id;
      $count = DB::table('User')->where('ID',$id)->count();
      if($count == 0){
        DB::table('User')->insert(
          ['ID'=>$id,'SNS_Relation'=>1]
        );
      }
      $c = DB::table('User')->where('ID',$id)->first();
      if($c->SNS_Relation == 0){
        $password = $request->password;
        if($c->Password != $password){
          //ID와 Password 가 실제 값과 일치하지 않다.
          $data = array(
                'success' => "1", // 실패
                'token' => "0",
            );
          return json_encode( $data );
        }else{
          $token = base64_encode($c->User_PK.".".hash('sha256',$id).".".date('Y-m-d H:i:s',time()));
          DB::update('update User set OurApp_Token = ? where user_PK = ?', [$token, $c->User_PK]);
          $data = array(
                'success' => "0", // 성공
                'token' => $token,
            );
          return json_encode( $data );
        }

      }else if($c->SNS_Relation == 1){
        $count = DB::table('User')->where('ID',$id)->count();
        $token = base64_encode($c->User_PK.".".hash('sha256',$id).".".date('Y-m-d H:i:s',time()));
        DB::update('update User set OurApp_Token = ?, Token =? where user_PK = ?', [$token, $request->token, $c->User_PK]);
        $data = array(
              'success' => "0", // 성공
              'token' => $token,
          );
        return json_encode( $data );
        // 다른 계정 연동해서 로그인 해야하는 부분이다.... 어떻게 처리할지 고민...
      }
    }

    public function CheckToken($request){
      $id = $request->id;
      $token = base64_decode($request->token);
      $explode_array = explode(".",$token);
      $User_PK = $explode_array[0];
      $Date = $explode_array[2];
      $ID_SHA256 = $explode_array[1];
      $c = DB::table('User')->where('User_PK',$User_PK)->first();
      if(hash($ID_SHA256) == $c->ID){
        $data = array(
              'success' => "0", // 성공
          );
        return json_encode( $data );
      }
      $data = array(
            'success' => "1", // 실패
        );
      return json_encode( $data );
    }

    public function SignUp(Request $request){
      // 일반 회원가입으로 회원가입 했을 때에는 가입에 성공시 success 변수에 0을 담아서 보낸다. 이 경우에는 id 까지 같이 보낸다
      // 회원가입에 실패한 경우에는 1을 담아서 보낸다.
      if($request->kind == 0){
        $id = $request->id;
        $password = $request->password;

        $count = DB::table('User')->where('ID',$id)->count();
        if($count == 0){
          DB::table('User')->insert(
            ['ID'=>$id,'Password'=>$password,'SNS_Relation'=>0]
          );
          $data = array(
                'success' => "0" // 성공
            );
          return json_encode( $data );
        }else{
          $data = array(
                'success' => "1" // 실패
            );
          return json_encode( $data );
        }
      }else if($request->kind == 1){
        $id = $request->id;
        $count = DB::table('User')->where('ID',$id)->count();
        DB::table('User')->insert(
          ['ID'=>$id,'SNS_Relation'=>1]
        );
        $data = array(
              'success' => "0" // 성공
          );
        return json_encode( $data );
      }
    }

    public function GetNickName(Request $request){
        $c = DB::table('User')->where('User_PK',$request->User_PK)->first();
          $data = array(
                'Success' => 0,
                'NickName' => $c->NickName,
              ); // 성공
          return json_encode( $data );
    }

    public function SetNickName(Request $request){
        DB::update('update User set NickName = ? where user_PK = ?', [$request->nickname, $request->User_PK]);
        $data = array(
              'Success' => 0, // 성공
          );
        return json_encode( $data );
    }

}

<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;

class CallData extends Controller
{
    public function GetCallData(Request $request){
      $data = DB::table('Backup_Calldata')->where('User_PK',$request['User_PK'])->get();
      $return_json_array = array();
      foreach($data as $d){
        $array = [
            "Name" => $d->Name,
            "Phone" => $d->Phone_Number,
        ];
        array_push($return_json_array,$array);
      }
      $data = array(
            'success' => "0", // 성공
            'phone' => $return_json_array,
        );
      return json_encode( $data );
    }

    public function SetCallData(Request $request){
      $phone = json_decode($request->phone);
      $insertDB = array();
      $User_PK = $request['User_PK'];
      if($request->method == "PUT" || $request->method == "put"){
        if(!empty($phone)){
          foreach($phone as $p){
              array_push($insertDB,[
                  'User_PK'=>$User_PK,
                  'Name'=>$p->Name,
                  'Phone_Number'=>$p->Phone
              ]);
          }
          DB::table('Backup_Calldata')->insert($insertDB);
          $data = array(
                'success' => "0" // 성공
          );
          return json_encode( $data );
        }
      }
      else if($request->method == "POST" || $request->method == "post"){
        return $this->UpdateCallData($request);
      }else{
        $data = array(
              'success' => "2" // method 정의되지 않음
        );
        return json_encode( $data );
      }
    }

    public function UpdateCallData(Request $request){
      $User_PK = $request['User_PK'];
      $phone = json_decode($request->phone);
      DB::table('Backup_Calldata')->where('User_PK',$User_PK)->delete();
      $insertDB = array();
      if(!empty($phone)){
        foreach($phone as $p){
            array_push($insertDB,[
                'User_PK'=>$User_PK,
                'Name'=>$p->Name,
                'Phone_Number'=>$p->Phone
            ]);
        }
        DB::table('Backup_Calldata')->insert($insertDB);
        $data = array(
              'success' => "0" // 성공
        );
        return json_encode( $data );
      }
    }
}

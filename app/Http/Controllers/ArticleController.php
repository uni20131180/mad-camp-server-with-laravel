<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    //
    public function index(){
      return "index";
    }
    public function show($id){
      return "show ".$id;
    }
    public function store(Request $request){
      return "create";
    }
    public function update(Request $request, $id){
      return "update ".$id;
    }
}

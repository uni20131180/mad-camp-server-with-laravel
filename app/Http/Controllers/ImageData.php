<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;

class ImageData extends Controller
{
    public function GetProfileImageData(Request $request){
        $r = DB::table('User')->where('User_PK',$request->User_PK)->first();
        if($r->Profile_Image == null || $r->Profile_Image == ""){
          $data = array(
                'success' => "3"
          );
          return json_encode( $data );
        }
        $data = array(
              'success' => "0",
              'imageURL' => "http://www.conkara.kr/storage/".$r->Profile_Image
        );
        return json_encode( $data );
    }
    public function SetProfileImageData(Request $request){
      $path = $request->file('imagefile')->storeAs(
          'profileimages', $request->User_PK."profileimage.jpg", 'public'
      );
      DB::update('update User set Profile_Image = ? where User_PK = ?', [$path, $request->User_PK]);
      $data = array(
            'success' => "0" // 업로드 성공... 실패는 어떻게 판단하지?
      );
      return json_encode( $data );
    }
    public function UpdateProfileImageData(Request $request){
      if($request->method == "POST"){
        return $this->SetProfileImageData($request);
      }else if($request->method == "PUT"){
        $path = $request->file('imagefile')->storeAs(
            'profileimages', $request->User_PK."profileimage.jpg", 'public'
        );
        DB::update('update User set Profile_Image = ? where User_PK = ?', [$path, $request->User_PK]);
        $data = array(
              'success' => "0" // 업로드 성공... 실패는 어떻게 판단하지?
        );
        return json_encode( $data );
      }else if($request->method == "DELETE"){
        return $this->DeleteProfileImageData($request);
      }else{
        $data = array(
              'success' => "2" // 업로드 실패... method 가 없다.
        );
        return json_encode( $data );
      }
    }
    public function DeleteProfileImageData(Request $request){
        $r = DB::table('User')->where('User_PK',$request->User_PK)->first();
        $s = Storage::delete("public/".$r->Profile_Image);
        if($s == false){
          $data = array(
                'success' => 1// 삭제 실패...
          );
          return json_encode( $data );
        }
        if($s == true){
          DB::update('update User set Profile_Image = ? where User_PK = ?', [null, $request->User_PK]);
          $data = array(
                'success' => 0// 삭제 성공
          );
          return json_encode( $data );
        }
    }
}

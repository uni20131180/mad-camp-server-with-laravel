<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class TokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($request->token)){
          $token = base64_decode($request->token);
          $explode_array = explode(".",$token);
          if(count($explode_array) == 3){
            $User_PK = $explode_array[0];
            $Date = $explode_array[2];
            $ID_SHA256 = $explode_array[1];
            $c = DB::table('User')->where('User_PK',$User_PK)->first();
            if($ID_SHA256 == hash("sha256",$c->ID)){
              if($request->token == $c->OurApp_Token){
                $request['User_PK']=$User_PK;
                return $next($request);
              }
            }
          }
        }
        redirect("/");
    }
}
